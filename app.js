const express = require('express');
const cors = require('cors');

const app = express();
app.use(express.json());
app.use(cors());

const PORT = 1337;

// In-memory store for messages
let messages = { 'murdoch': {}, 'jon': {} };
let users = { 'murdoch': 'key1', 'jon': 'key2' };

// Associate your userid with a public key 
app.post("/register", (request, response) => {
    const { userid, public_key } = request.body;

    if (!userid || !public_key) {
        return response.status(400).send("Invalid request body");
    }

    if (userid in users && JSON.stringify(users[userid]) === JSON.stringify(public_key)) {
        // User has registered with the same key before.
        // TODO: There should be some kind of authentication here.
        return response.status(200).send("Re-registered successfully.");
    } else if (userid in users) {
        return response.status(400).send("User already exists");
    }

    users[userid] = public_key;
    messages[userid] = {};
    console.log(users);

    return response.status(200).send("Registered successfully");
});

// Get the public key for a userid 
app.post("/get_key", (request, response) => {
    const { userid } = request.body;

    if (!userid) {
        return response.status(400).send("Invalid request body");
    }

    if (!(userid in users)) {
        return response.status(400).send("User does not exist");
    }


    return response.status(200).json({ public_key: users[userid] });
});

// Send 
app.post("/send", (request, response) => {
    const { senderid, recipientid, encrypted_text } = request.body;
    console.log(senderid, recipientid, encrypted_text);

    if (!senderid || !recipientid || !encrypted_text) {
        console.log("[-] Send: invalid request body");
        return response.status(400).send("Invalid request body");
    }

    if (!(senderid in users) || !(recipientid in users)) {
        console.log("[-] Send: One of these users does not exist");
        return response.status(400).send("One of these users does not exist");
    }

    const timestamp = Date.now();
    const newMessage = {
        senderid,
        recipientid,
        encrypted_text,
        timestamp
    };

    if (!(senderid in messages[recipientid])) {
        messages[recipientid][senderid] = [newMessage];
    } else {
        messages[recipientid][senderid].push(newMessage);
    }

    console.log(messages);

    return response.status(200).send("Message sent");
});

// Get list of senders to user userid
// TODO: Should also return messages send from userid without receiving a response
app.post("/get_chats", (request, response) => {
    const { userid } = request.body;

    if (!userid) {
        console.log("[-] get_chats: Invalid request body");
        return response.status(400).send("Invalid request body");
    }

    if (!(userid in users)) {
        console.log("[-] get_chats: user " + userid + " does not exist");
        return response.status(400).send("User does not exist");
    }
    
    // Get all people user has messaged
    let chats = new Set();
    for (let recipient in messages) {
        if (messages[recipient][userid]) {
            chats.add(recipient);
        }
    }

    // Get all people user has been messaged by
    let all_chats = chats.union(new Set(Object.keys(messages[userid])));
    
    // return response.status(200).json({ senders: Object.keys(messages[userid]) });
    return response.status(200).json({ senders: Array.from(all_chats) });
});

// Get messages given a recipient userid and senderid
// TODO: Optional parameter for messages since <time> -- this will help restrict
// how much data is sent in each packet
app.post("/get_messages", (request, response) => {
    const { userid, senderid } = request.body;
    console.log("[-] Get Messages: " + userid + ", " + senderid);

    if (!userid || !senderid) {
        console.log("[-] Get Messages: invalid request body");
        return response.status(400).send("Invalid request body");
    }

    if (!(userid in users) || !(senderid in users)) {
        console.log("[-] Get Messages: One of these users does not exist");
        return response.status(400).send("One of these users does not exist");
    }

    return response.status(200).json({ messages: messages[userid][senderid] });
});

app.post("/get_chat_history", (request, response) => {
    const { user1, user2 } = request.body;
    console.log("[+] Get Chat History: " + user1 + ", " + user2);

    if (!user1 || !user2) {
        console.log("[-] Get Chat History: invalid request body");
        return response.status(400).send("Invalid request body");
    }

    if (!(user1 in users) || !(user2 in users)) {
        console.log("[-] Get Chat History: One of these users does not exist");
        return response.status(400).send("One of these users does not exist");
    }

    // let history;
    if (user1 === user2) {
        let u1_u2 = messages[user2][user1] || [];
        return response.status(200).json({ messages: u1_u2 });
    } 
    let u1_u2 = messages[user2][user1] || [];
    let u2_u1 = messages[user1][user2] || [];
    let history = u1_u2.concat(u2_u1);    
    history.sort((m1, m2) => m1.timestamp - m2.timestamp);
    return response.status(200).json({ messages: history });
});

app.listen(PORT, () => {
    console.log("Server Listening on PORT:", PORT);
});

